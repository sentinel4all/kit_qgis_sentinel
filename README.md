| <img src="./Pictures/ONFi_logotype_A_pantone.png" alt="drawing" height="60"/> | <img src="./Pictures/logo_lastig.png" alt="drawing" height="65"/><img src="./Pictures/Logo_gustave_eiffel.jpg" alt="drawing" height="60"/> |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| clardeux@gmail.com<br />cedric.lardeux@onfinternational.com  |                 pierre-louis.frison@u-pem.fr                 |


# Kit QGIS Sentinel

Lot of people concerned in environmental studies can be interested in remote sensing Sentinel data. This can be tricky for those who are not experts in image processing techniques and computer sciences.

This kit has been especialy designed for them.

It consists in QGIS 3.16 software wich integrates some basic processing for Sentinel data. With these additional plugins, you can, for example, calibrate and orthorectify a Sentinel-1 temporal series over a region of interest. This is available in the plugin ***Sentinel for All***

![](./Pictures/screenshot_processing_plugin.png)

This plugin is based on the following open source libs: 

​		**Orfeo ToolBox** https://www.orfeo-toolbox.org 

​		**RIOS** 

​		and **python libraries** used by the codes we developed

## 

## How to install it ?

- Download QGIS_Sentinel_installer.exe
- run it (accept all the requests)

The kit is now installed.

Run the QGIS_Sentinel shortcut on your Desktop to tun the QGIS kit.

## To do what ?

### Sentinel-1 processing

- Because Sentinel-1 data are freely available but not delivered on a format fully usable as Sentinel-2 (ortorectified, ...) we develop a tools that get Sentinel-1 zip files (GRD format) and on shape file delimiting the area and that process all the data. The output data are calibrated and orthorectified.  <u>**Tutorial Link TO DO.**</u>
- In order to remove the speckle we devellop a tools that apply a temporal speckle filter. <u>**Tutorial Link TO DO.**</u>

### Classification

The classification tools is based on the Orfeo ToolBox and allow user to create a RandomForest classification

## The team

- Cédric Lardeux (ONF International,  http://www.onfinternational.org)
- Pierre-Louis Frison  (Gustave Eiffel University, https://igm.u-pem.fr/presentation/)

## 
